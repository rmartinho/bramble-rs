//! BRP transport support

use crate::Result;
use bramble_common::transport::Id;
use bramble_crypto::{Role, SymmetricKey};
use futures::{AsyncRead, AsyncWrite, Future};

/// A rendezvous transport
pub trait Rendezvous: Id {
    /// The type of connections established over this transport.
    type Connection: AsyncRead + AsyncWrite;

    /// The future returned by [`listen`](Self::listen).
    type ListenFuture: Future<Output = Result<Self::Connection>>;
    /// The future returned by [`connect`](Self::connect).
    type ConnectFuture: Future<Output = Result<Self::Connection>>;

    /// Prepares the endpoints for the connection using the given stream key.
    fn prepare_endpoints(&mut self, stream_key: SymmetricKey, role: Role);

    /// Listens for a connection in one of the previously established endpoints.
    fn listen(&mut self) -> Self::ListenFuture;

    /// Performs one connection attempt using the previously established endpoints.
    fn connect(&mut self) -> Self::ConnectFuture;
}
