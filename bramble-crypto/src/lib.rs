//! The cryptographic primitives used in the Bramble protocols
#![warn(missing_docs)]

mod error;
mod primitives;
mod role;

pub use bramble_common::{Error, Result};
pub use primitives::{
    dec, dh, enc, hash, kdf, kex, mac, prf, stream, Hash, KeyPair, Mac, Nonce, PublicKey,
    SecretKey, SymmetricKey, AUTH_LEN, HASH_LEN, KEY_LEN, MAC_LEN, NONCE_LEN, PRF_LEN,
};
pub use role::Role;
