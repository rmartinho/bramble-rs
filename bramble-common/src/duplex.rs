//! Duplex pipes for testing

use crate::transport::{Id, Latency};
use futures::{
    io,
    lock::BiLock,
    ready,
    task::{AtomicWaker, Context, Poll},
    AsyncRead, AsyncWrite,
};
use pin_project::pin_project;
use std::{cmp::min as at_most, collections::VecDeque, pin::Pin, sync::Arc};

/// The read end of a pipe
#[pin_project]
pub struct ReadPipe {
    #[pin]
    buf: BiLock<VecDeque<u8>>,
    waker: Arc<AtomicWaker>,
}

/// The write end of a pipe
#[pin_project]
pub struct WritePipe {
    #[pin]
    buf: BiLock<VecDeque<u8>>,
    waker: Arc<AtomicWaker>,
}

/// Creates a new pipe
pub fn make_pipe() -> (ReadPipe, WritePipe) {
    let bufs = BiLock::new(VecDeque::new());
    let waker = Arc::new(AtomicWaker::new());
    (
        ReadPipe {
            buf: bufs.0,
            waker: waker.clone(),
        },
        WritePipe { buf: bufs.1, waker },
    )
}

impl AsyncRead for ReadPipe {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &mut [u8],
    ) -> Poll<IoResult<usize>> {
        if buf.is_empty() {
            return Poll::Ready(Ok(0));
        }
        let this = self.as_mut().project();
        this.waker.register(cx.waker());
        let mut data = ready!(this.buf.poll_lock(cx));
        let (slice, _) = data.as_slices();
        let available = slice.len();
        if available == 0 {
            return Poll::Pending;
        }
        let to_read = at_most(available, buf.len());
        buf[..to_read].copy_from_slice(&slice[..to_read]);
        data.drain(..to_read);
        Poll::Ready(Ok(to_read))
    }
}

impl AsyncWrite for WritePipe {
    fn poll_write(mut self: Pin<&mut Self>, cx: &mut Context, buf: &[u8]) -> Poll<IoResult<usize>> {
        if buf.is_empty() {
            return Poll::Ready(Ok(0));
        }
        let this = self.as_mut().project();
        let mut data = ready!(this.buf.poll_lock(cx));
        data.extend(buf);
        this.waker.wake();
        Poll::Ready(Ok(buf.len()))
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<IoResult<()>> {
        Poll::Ready(Ok(()))
    }

    fn poll_close(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<IoResult<()>> {
        Poll::Ready(Ok(()))
    }
}

/// One end of a duplex pipe
#[pin_project]
pub struct Duplex {
    #[pin]
    reader: ReadPipe,
    #[pin]
    writer: WritePipe,
}

impl AsyncRead for Duplex {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &mut [u8],
    ) -> Poll<IoResult<usize>> {
        self.as_mut().project().reader.poll_read(cx, buf)
    }
}

impl AsyncWrite for Duplex {
    fn poll_write(mut self: Pin<&mut Self>, cx: &mut Context, buf: &[u8]) -> Poll<IoResult<usize>> {
        self.as_mut().project().writer.poll_write(cx, buf)
    }
    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<IoResult<()>> {
        self.as_mut().project().writer.poll_flush(cx)
    }
    fn poll_close(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<IoResult<()>> {
        self.as_mut().project().writer.poll_close(cx)
    }
}

impl Id for Duplex {
    const ID: &'static [u8] = b"test.transport.duplex";
}

impl Latency for Duplex {
    const MAX_LATENCY_SECONDS: u32 = 1;
}

/// Creates a new pipe-based duplex transport
pub fn make_duplex() -> (Duplex, Duplex) {
    let (read2, write1) = make_pipe();
    let (read1, write2) = make_pipe();

    (
        Duplex {
            reader: read1,
            writer: write1,
        },
        Duplex {
            reader: read2,
            writer: write2,
        },
    )
}

type IoResult<T> = std::result::Result<T, io::Error>;
