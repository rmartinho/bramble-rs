//! Timer utils

use crate::{Error, Result};
use async_io::Timer;
use futures::{ready, Future};
use pin_project::pin_project;
use std::time::Duration;
use std::{
    pin::Pin,
    task::{Context, Poll},
};

/// Sleeps for the specified amount of time.
pub async fn sleep(duration: Duration) {
    let _ = Timer::after(duration).await;
}

/// Awaits a future or times out after a duration of time.
pub async fn timeout<F, T>(duration: Duration, f: F) -> Result<T>
where
    F: Future<Output = Result<T>>,
{
    TimeoutFuture::new(duration, f).await
}

#[pin_project]
struct TimeoutFuture<F> {
    #[pin]
    future: F,
    #[pin]
    delay: Timer,
}

impl<F> TimeoutFuture<F> {
    fn new(duration: Duration, future: F) -> TimeoutFuture<F> {
        TimeoutFuture {
            future,
            delay: Timer::after(duration),
        }
    }
}

impl<F, T> Future for TimeoutFuture<F>
where
    F: Future<Output = Result<T>>,
{
    type Output = Result<T>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        let this = self.project();
        match this.future.poll(cx) {
            Poll::Pending => {
                let _ = ready!(this.delay.poll(cx));
                Poll::Ready(Err(Error::Timeout))
            }
            r => r,
        }
    }
}
