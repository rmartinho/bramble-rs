//! Bramble errors

use serde::{
    de::{self, Expected, Unexpected},
    ser,
};
use std::{char::CharTryFromError, fmt::Display, io, num::TryFromIntError};
use thiserror::Error;

/// Bramble errors
#[derive(Debug, Error)]
pub enum Error {
    /// An I/O error has occurred.
    #[error(transparent)]
    Io(#[from] io::Error),

    /// An invalid MAC was detected.
    #[error("invalid MAC")]
    InvalidMac,
    /// A key exchange failed.
    #[error("key exchange failure")]
    FailedKeyExchange,
    /// Random number generation failed,
    #[error("RNG failure")]
    FailedRng,
    /// Trailing bytes found in the stream.
    #[error("unexpected trailing bytes")]
    TrailingBytes,
    /// An object with a different type was expected.
    #[error("encountered wrong data type")]
    WrongType, // TODO more details
    /// An invalid object type was found.
    #[error("invalid data type")]
    InvalidType,
    /// An invalid object value was found.
    #[error("invalid value")]
    InvalidValue,
    /// An invalid object length was found.
    #[error("invalid length")]
    InvalidLength,
    /// An invalid object length-of-length was found.
    #[error("invalid length of length")]
    InvalidLengthOfLength,
    /// An older protocol version was found.
    #[error("older version found")]
    OlderVersion,
    /// A beta protocol version was found.
    #[error("beta version found")]
    BetaVersion,
    /// A newer protocol version was found.
    #[error("newer version found")]
    NewerVersion,
    /// An invalid record was received.
    #[error("invalid record")]
    InvalidRecord,
    /// An invalid public key was found.
    #[error("invalid key found")]
    InvalidKey,
    /// An invalid proof of ownership was found.
    #[error("invalid proof of ownership found")]
    InvalidProof,
    /// An empty payload was found.
    #[error("empty payload")]
    EmptyPayload,
    /// An invalid commitment was found.
    #[error("invalid commitment")]
    InvalidCommitment,
    /// An invalid transport was found.
    #[error("invalid transport")]
    InvalidTransport,
    /// An invalid confirmation was received.
    #[error("invalid confirmation")]
    InvalidConfirmation,
    /// Time period in the past.
    #[error("past time period requested")]
    TimePeriodIsPast,
    /// Time has ran out.
    #[error("time has ran out")]
    Timeout,

    #[doc(hidden)]
    #[error("{0:}")]
    Other(String),
}

impl Error {
    /// Helper for creating EOF I/O errors
    pub fn eof() -> Error {
        Self::from(io::Error::new(
            io::ErrorKind::UnexpectedEof,
            "unexpected end of input",
        ))
    }

    /// Helper for matching EOF I/O errors
    pub fn is_eof(&self) -> bool {
        matches!(self, Error::Io(e) if e.kind() == io::ErrorKind::UnexpectedEof)
    }
}

impl From<TryFromIntError> for Error {
    fn from(_: TryFromIntError) -> Self {
        Self::InvalidValue
    }
}

impl From<CharTryFromError> for Error {
    fn from(_: CharTryFromError) -> Self {
        Self::InvalidValue
    }
}

impl From<rand::Error> for Error {
    fn from(_: rand::Error) -> Self {
        Self::FailedRng
    }
}

impl From<xsalsa20poly1305::aead::Error> for Error {
    fn from(_: xsalsa20poly1305::aead::Error) -> Self {
        Self::InvalidMac
    }
}

impl ser::Error for Error {
    fn custom<T: Display>(msg: T) -> Self {
        Self::Other(msg.to_string())
    }
}

impl de::Error for Error {
    fn custom<T: Display>(msg: T) -> Self {
        Self::Other(msg.to_string())
    }

    fn invalid_type(_unexp: Unexpected, _exp: &dyn Expected) -> Self {
        Self::WrongType
    }

    fn invalid_value(_unexp: Unexpected, _exp: &dyn Expected) -> Self {
        Self::InvalidValue
    }

    fn invalid_length(_len: usize, _exp: &dyn Expected) -> Self {
        Self::InvalidLength
    }
}

/// Result type for Bramble errors.
pub type Result<T> = std::result::Result<T, Error>;
