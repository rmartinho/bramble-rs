//! BTP streams

use crate::{
    clock::{Clock, Utc},
    crypto::{KeyManager, StreamId, Tag},
    Result, CURRENT_VERSION,
};
use bramble_common::transport::{Id, Latency};
use bramble_crypto::{dec, enc, Nonce, Role, SymmetricKey, AUTH_LEN, KEY_LEN, NONCE_LEN};
use futures::{
    io, ready,
    task::{Context, Poll},
    AsyncRead, AsyncWrite,
};
use pin_project::pin_project;
use rand::thread_rng;
use std::{cmp::min as at_most, collections::VecDeque, pin::Pin};

/// A marker for secure transports
pub trait Secure {}

/// A BTP connection
#[pin_project]
pub struct Connection<T, C = Utc>
where
    T: Id + Latency + AsyncRead + AsyncWrite,
    C: Clock,
{
    #[pin]
    transport: T,
    keys: KeyManager<T>,
    clock: C,

    reader_state: ReaderState,
    writer_state: WriterState,
}

impl<T> Secure for Connection<T, Utc> where T: Id + Latency + AsyncRead + AsyncWrite {}

impl<T> Id for Connection<T, Utc>
where
    T: Id + Latency + AsyncRead + AsyncWrite,
{
    const ID: &'static [u8] = T::ID;
}

impl<T> Latency for Connection<T, Utc>
where
    T: Id + Latency + AsyncRead + AsyncWrite,
{
    const MAX_LATENCY_SECONDS: u32 = T::MAX_LATENCY_SECONDS;
}

struct ReaderState {
    buf: VecDeque<u8>,
    state: ReadState,
    frame_number: u64,
    frame_key: Option<SymmetricKey>,
    final_frame: bool,
}

struct WriterState {
    buf: VecDeque<u8>,
    state: WriteState,
    frame_number: u64,
    frame_key: Option<SymmetricKey>,
}

#[derive(PartialEq, Eq, Copy, Clone, Debug)]
enum ReadState {
    Tag {
        read: usize,
    },
    StreamHeader {
        read: usize,
        stream_id: StreamId,
        header_key: SymmetricKey,
    },
    HeaderCiphertext {
        read: usize,
    },
    BodyCiphertext {
        data: usize,
        padding: usize,
        read: usize,
    },
    Plaintext,
    Closed,
}

#[derive(PartialEq, Eq, Copy, Clone, Debug)]
enum WriteState {
    StreamHeader,
    Plaintext,
    Ciphertext,
    Closed,
}

impl<T, C> AsyncRead for Connection<T, C>
where
    T: Id + Latency + AsyncRead + AsyncWrite,
    C: Clock,
{
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &mut [u8],
    ) -> Poll<IoResult<usize>> {
        self.assert_reader_not_closed()?;
        if buf.is_empty() {
            return Poll::Ready(Ok(0));
        }
        loop {
            let this = self.as_mut().project();
            match this.reader_state.state {
                ReadState::Tag { ref mut read } => {
                    this.reader_state.buf.resize(TAG_LEN, 0);
                    let slice = &mut this.reader_state.buf.make_contiguous()[*read..];
                    *read += ready!(this.transport.poll_read(cx, slice))?;
                    if *read == TAG_LEN {
                        self.check_tag()?;
                    }
                }
                ReadState::StreamHeader { ref mut read, .. } => {
                    this.reader_state.buf.resize(STREAM_HEADER_LEN, 0);
                    let slice = &mut this.reader_state.buf.make_contiguous()[*read..];
                    *read += ready!(this.transport.poll_read(cx, slice))?;
                    if *read == STREAM_HEADER_LEN {
                        self.parse_stream_header()?;
                    }
                }
                ReadState::HeaderCiphertext { ref mut read } => {
                    this.reader_state.buf.resize(FRAME_HEADER_LEN, 0);
                    let slice = &mut this.reader_state.buf.make_contiguous()[*read..];
                    *read += ready!(this.transport.poll_read(cx, slice))?;
                    if *read == FRAME_HEADER_LEN {
                        self.decrypt_header()?;
                    }
                }
                ReadState::BodyCiphertext {
                    data,
                    padding,
                    ref mut read,
                } => {
                    this.reader_state.buf.resize(data + padding + AUTH_LEN, 0);
                    let slice = &mut this.reader_state.buf.make_contiguous()[*read..];
                    *read += ready!(this.transport.poll_read(cx, slice))?;
                    if *read == data + padding + AUTH_LEN {
                        self.decrypt_body()?;
                    }
                }
                ReadState::Plaintext => {
                    let available = this.reader_state.buf.len();
                    let to_read = at_most(available, buf.len());
                    let slice = &this.reader_state.buf.make_contiguous()[..to_read];
                    buf[..to_read].copy_from_slice(slice);
                    this.reader_state.buf.drain(..to_read);
                    if this.reader_state.buf.is_empty() {
                        if this.reader_state.final_frame {
                            self.set_reader_closed();
                        } else {
                            this.reader_state.state = ReadState::HeaderCiphertext { read: 0 };
                            this.reader_state.frame_number += 1;
                        }
                    }
                    break Poll::Ready(Ok(to_read));
                }
                ReadState::Closed => unreachable!(),
            }
        }
    }
}

impl<T, C> AsyncWrite for Connection<T, C>
where
    T: Id + Latency + AsyncRead + AsyncWrite,
    C: Clock,
{
    fn poll_write(mut self: Pin<&mut Self>, cx: &mut Context, buf: &[u8]) -> Poll<IoResult<usize>> {
        self.assert_writer_not_closed()?;
        if buf.is_empty() {
            return Poll::Ready(Ok(0));
        }
        loop {
            let this = self.as_mut().project();
            let can_write =
                FRAME_HEADER_LEN + MAX_FRAME_PLAINTEXT_LEN > this.writer_state.buf.len();
            match this.writer_state.state {
                WriteState::StreamHeader => {
                    let (slice, _) = this.writer_state.buf.as_slices();
                    let written = ready!(this.transport.poll_write(cx, slice))?;
                    this.writer_state.buf.drain(..written);
                    if this.writer_state.buf.is_empty() {
                        this.writer_state.buf.resize(FRAME_HEADER_LEN, 0);
                        this.writer_state.state = WriteState::Plaintext;
                    }
                }
                WriteState::Plaintext if can_write => {
                    let available =
                        FRAME_HEADER_LEN + MAX_FRAME_PLAINTEXT_LEN - this.writer_state.buf.len();
                    let to_write = at_most(available, buf.len());
                    this.writer_state.buf.extend(&buf[..to_write]);
                    break Poll::Ready(Ok(to_write));
                }
                WriteState::Closed => unreachable!(),
                _ => {
                    let _ = ready!(self.as_mut().poll_flush(cx));
                }
            }
        }
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<IoResult<()>> {
        self.assert_writer_not_closed()?;
        let is_plain = matches!(self.writer_state.state, WriteState::Plaintext);
        if is_plain {
            self.encrypt(false);
        }
        loop {
            let mut this = self.as_mut().project();
            if this.writer_state.buf.is_empty() {
                let _ = ready!(this.transport.poll_flush(cx));
                this.writer_state.state = WriteState::Plaintext;
                this.writer_state.frame_number += 1;
                this.writer_state.buf.resize(FRAME_HEADER_LEN, 0);
                break Poll::Ready(Ok(()));
            } else {
                let (slice, _) = this.writer_state.buf.as_slices();
                let written = ready!(this.transport.as_mut().poll_write(cx, slice))?;
                this.writer_state.buf.drain(..written);
            }
        }
    }

    fn poll_close(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<IoResult<()>> {
        self.assert_writer_not_closed()?;
        let is_plain = matches!(self.writer_state.state, WriteState::Plaintext);
        if is_plain {
            self.encrypt(true);
        }
        loop {
            let this = self.as_mut().project();
            // flushing prepares a new header so this means the buffer is empty
            if this.writer_state.buf.len() == FRAME_HEADER_LEN {
                let _ = ready!(this.transport.poll_close(cx));
                self.set_writer_closed();
                break Poll::Ready(Ok(()));
            } else {
                let _ = ready!(self.as_mut().poll_flush(cx));
            }
        }
    }
}

impl<T> Connection<T, Utc>
where
    T: Id + Latency + AsyncRead + AsyncWrite,
{
    fn new(transport: T, clock: Utc, keys: KeyManager<T>, send_number: u64) -> Result<Self> {
        let mut stream = Self {
            transport,
            clock,
            keys,
            reader_state: ReaderState {
                buf: VecDeque::new(),
                state: ReadState::Tag { read: 0 },
                frame_key: None,
                frame_number: 0,
                final_frame: false,
            },
            writer_state: WriterState {
                buf: VecDeque::new(),
                state: WriteState::StreamHeader,
                frame_key: None,
                frame_number: 0,
            },
        };
        stream.make_stream_header(send_number)?;
        Ok(stream)
    }

    /// Creates a new BTP stream in handshake mode.
    pub fn handshake(
        transport: T,
        root_key: SymmetricKey,
        role: Role,
        send_number: u64,
    ) -> Result<Self> {
        let period_len = T::MAX_LATENCY_SECONDS + MAX_CLOCK_DIFFERENCE_SECONDS;
        let clock = Utc::new(period_len);
        let now = clock.now();
        let keys = KeyManager::handshake(root_key, now, role)?;
        Self::new(transport, clock, keys, send_number)
    }

    /// Creates a new BTP stream in rotation mode.
    pub fn rotation(
        transport: T,
        root_key: SymmetricKey,
        role: Role,
        send_number: u64,
    ) -> Result<Self> {
        let period_len = T::MAX_LATENCY_SECONDS + MAX_CLOCK_DIFFERENCE_SECONDS;
        let clock = Utc::new(period_len);
        let now = clock.now();
        let keys = KeyManager::rotation(root_key, now, role)?;
        Self::new(transport, clock, keys, send_number)
    }
}

impl<T, C> Connection<T, C>
where
    T: Id + Latency + AsyncRead + AsyncWrite,
    C: Clock,
{
    fn check_tag(self: &mut Pin<&mut Self>) -> IoResult<()> {
        let this = self.as_mut().project();

        let now = this.clock.now();
        this.keys
            .advance_to(now)
            .map_err(|_| io::Error::new(io::ErrorKind::Other, "key rotation failed"))?;
        let mut buf = [0u8; TAG_LEN];
        buf.copy_from_slice(&this.reader_state.buf.make_contiguous()[..TAG_LEN]);
        let tag = Tag::from(buf);
        let (stream_id, keychain) = this
            .keys
            .see_tag(tag)
            .ok_or_else(|| io::Error::new(io::ErrorKind::ConnectionRefused, "invalid stream"))?;
        this.reader_state.buf.clear();
        this.reader_state.state = ReadState::StreamHeader {
            read: 0,
            stream_id,
            header_key: *keychain.header_key(),
        };
        Ok(())
    }

    fn parse_stream_header(self: &mut Pin<&mut Self>) -> IoResult<()> {
        let this = self.as_mut().project();
        let (nonce, ciphertext) = this
            .reader_state
            .buf
            .make_contiguous()
            .split_at_mut(NONCE_LEN);
        let (ciphertext, auth_tag) = ciphertext.split_at_mut(ciphertext.len() - AUTH_LEN);

        let mut buf = [0u8; NONCE_LEN];
        buf.copy_from_slice(nonce);
        let nonce = Nonce::from(buf);

        let (stream_id, header_key) = match this.reader_state.state {
            ReadState::StreamHeader {
                stream_id,
                header_key,
                ..
            } => (stream_id, header_key),
            _ => unreachable!(),
        };
        dec(&header_key, &nonce, ciphertext, auth_tag)
            .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "authentication failed"))?;
        let plaintext = ciphertext;
        let (version, plaintext) = plaintext.split_at(2);
        let (stream_number, frame_key) = plaintext.split_at(8);

        let mut buf = [0u8; 2];
        buf.copy_from_slice(version);
        if u16::from_be_bytes(buf) != CURRENT_VERSION {
            return Err(io::Error::new(
                io::ErrorKind::ConnectionRefused,
                "invalid stream",
            ));
        }

        let mut buf = [0u8; 8];
        buf.copy_from_slice(stream_number);
        if u64::from_be_bytes(buf) != stream_id.number {
            return Err(io::Error::new(
                io::ErrorKind::ConnectionRefused,
                "invalid stream",
            ));
        }

        let mut buf = [0u8; KEY_LEN];
        buf.copy_from_slice(frame_key);

        this.reader_state.frame_key = Some(SymmetricKey::from(buf));
        this.reader_state.buf.clear();
        this.reader_state.state = ReadState::HeaderCiphertext { read: 0 };
        Ok(())
    }

    fn decrypt_header(self: &mut Pin<&mut Self>) -> IoResult<()> {
        let this = self.as_mut().project();

        let mut buf = [0u8; NONCE_LEN];
        buf[..8].copy_from_slice(&this.reader_state.frame_number.to_be_bytes());
        buf[0] |= FRAME_HEADER_FLAG;
        let nonce = Nonce::from(buf);

        let frame_key = this.reader_state.frame_key.as_ref().unwrap();
        let (ciphertext, auth_tag) = this
            .reader_state
            .buf
            .make_contiguous()
            .split_at_mut(FRAME_HEADER_PLAINTEXT_LEN);
        dec(frame_key, &nonce, ciphertext, auth_tag)
            .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "authentication failed"))?;

        let (data, padding) = ciphertext.split_at(2);
        let mut buf = [0u8; 2];
        buf.copy_from_slice(data);
        let mut data = u16::from_be_bytes(buf);
        let mut buf = [0u8; 2];
        buf.copy_from_slice(padding);
        let padding = u16::from_be_bytes(buf);

        if data & FINAL_FRAME_FLAG != 0 {
            this.reader_state.final_frame = true;
            data &= !FINAL_FRAME_FLAG;
        }

        this.reader_state.buf.clear();
        this.reader_state.state = ReadState::BodyCiphertext {
            data: data as _,
            padding: padding as _,
            read: 0,
        };
        Ok(())
    }

    fn decrypt_body(self: &mut Pin<&mut Self>) -> IoResult<()> {
        let this = self.as_mut().project();

        let (data, padding) = match this.reader_state.state {
            ReadState::BodyCiphertext { data, padding, .. } => (data, padding),
            _ => unreachable!(),
        };

        let mut buf = [0u8; NONCE_LEN];
        buf[..8].copy_from_slice(&this.reader_state.frame_number.to_be_bytes());
        let nonce = Nonce::from(buf);

        let frame_key = this.reader_state.frame_key.as_ref().unwrap();
        let (ciphertext, auth_tag) = this
            .reader_state
            .buf
            .make_contiguous()
            .split_at_mut(data + padding);
        dec(frame_key, &nonce, ciphertext, auth_tag)
            .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "authentication failed"))?;

        this.reader_state.buf.truncate(data);
        this.reader_state.state = ReadState::Plaintext;
        Ok(())
    }

    fn encrypt(self: &mut Pin<&mut Self>, final_frame: bool) {
        let this = self.as_mut().project();

        this.writer_state
            .buf
            .resize(this.writer_state.buf.len() + AUTH_LEN, 0);
        let slice = this.writer_state.buf.make_contiguous();

        let data_len = slice.len() - FRAME_HEADER_LEN - AUTH_LEN;
        let (header_buf, slice) = slice.split_at_mut(FRAME_HEADER_LEN);
        let (plaintext, body_auth_tag) = slice.split_at_mut(data_len);

        let mut buf = [0u8; NONCE_LEN];
        buf[..8].copy_from_slice(&this.writer_state.frame_number.to_be_bytes());
        let body_nonce = Nonce::from(buf);
        buf[0] |= FRAME_HEADER_FLAG;
        let header_nonce = Nonce::from(buf);

        let (header_plaintext, header_auth_tag) =
            header_buf.split_at_mut(FRAME_HEADER_PLAINTEXT_LEN);

        let (data_buf, padding_buf) = header_plaintext.split_at_mut(2);
        let mut data_word = data_len as u16;
        if final_frame {
            data_word |= FINAL_FRAME_FLAG;
        }
        data_buf.copy_from_slice(&data_word.to_be_bytes());
        padding_buf.copy_from_slice(&0u16.to_be_bytes());

        let frame_key = this.writer_state.frame_key.as_ref().unwrap();
        enc(frame_key, &header_nonce, header_plaintext, header_auth_tag);
        enc(frame_key, &body_nonce, plaintext, body_auth_tag);

        this.writer_state.state = WriteState::Ciphertext;
    }

    fn make_stream_header(&mut self, stream_number: u64) -> Result<()> {
        self.writer_state.buf.resize(TAG_LEN + STREAM_HEADER_LEN, 0);
        let slice = self.writer_state.buf.make_contiguous();
        let (tag_buf, slice) = slice.split_at_mut(TAG_LEN);
        let (nonce_buf, slice) = slice.split_at_mut(NONCE_LEN);
        let (plaintext, auth_tag) = slice.split_at_mut(STREAM_HEADER_PLAINTEXT_LEN);
        let (version_buf, slice) = plaintext.split_at_mut(2);
        let (stream_number_buf, frame_key_buf) = slice.split_at_mut(8);

        let keys = self.keys.outgoing_keys_for(self.clock.now())?;

        let tag = Tag::new(keys.tag_key(), stream_number);
        tag_buf.copy_from_slice(tag.as_ref());

        let nonce = Nonce::new(&mut thread_rng());
        nonce_buf.copy_from_slice(nonce.as_ref());
        version_buf.copy_from_slice(&CURRENT_VERSION.to_be_bytes());
        stream_number_buf.copy_from_slice(&stream_number.to_be_bytes());

        let frame_key = SymmetricKey::generate(&mut thread_rng());
        frame_key_buf.copy_from_slice(frame_key.as_ref());
        self.writer_state.frame_key = Some(frame_key);

        enc(keys.header_key(), &nonce, plaintext, auth_tag);

        Ok(())
    }

    fn assert_reader_not_closed(&self) -> IoResult<()> {
        if matches!(self.reader_state.state, ReadState::Closed) {
            Err(io::Error::new(
                io::ErrorKind::BrokenPipe,
                "stream reader was closed",
            ))
        } else {
            Ok(())
        }
    }

    fn set_reader_closed(self: &mut Pin<&mut Self>) {
        let this = self.as_mut().project();

        this.reader_state.frame_key = None;
        this.reader_state.state = ReadState::Closed;
    }

    fn assert_writer_not_closed(&self) -> IoResult<()> {
        if matches!(self.writer_state.state, WriteState::Closed) {
            Err(io::Error::new(
                io::ErrorKind::BrokenPipe,
                "stream writer was closed",
            ))
        } else {
            Ok(())
        }
    }

    fn set_writer_closed(self: &mut Pin<&mut Self>) {
        let this = self.as_mut().project();

        this.writer_state.frame_key = None;
        this.writer_state.state = WriteState::Closed;
    }
}

type IoResult<T> = std::result::Result<T, io::Error>;

const TAG_LEN: usize = 16;

const STREAM_HEADER_PLAINTEXT_LEN: usize = 2 + 8 + KEY_LEN;
const STREAM_HEADER_LEN: usize = NONCE_LEN + STREAM_HEADER_PLAINTEXT_LEN + AUTH_LEN;

const FRAME_HEADER_PLAINTEXT_LEN: usize = 4;
const FRAME_HEADER_LEN: usize = FRAME_HEADER_PLAINTEXT_LEN + AUTH_LEN;

const MAX_FRAME_PLAINTEXT_LEN: usize = 1 << 15;

const MAX_CLOCK_DIFFERENCE_SECONDS: u32 = 24 * 60 * 60;

const FRAME_HEADER_FLAG: u8 = 0b1000_0000;
const FINAL_FRAME_FLAG: u16 = 0b1000_0000_0000_0000;

// TODO check data length limits
// TODO check frame count limits
// TODO close streams/delete keys on error

#[cfg(test)]
mod test {
    use super::*;
    use bramble_common::make_duplex;
    use futures::{executor::block_on, join};
    use rand::thread_rng;

    #[test]
    fn writer_and_reader_work_together() {
        let rng = &mut thread_rng();
        let root_key = SymmetricKey::generate(rng);
        let (transport_a, transport_b) = make_duplex();
        let mut stream_a = Connection::rotation(transport_a, root_key, Role::Alice, 17).unwrap();
        let mut stream_b = Connection::rotation(transport_b, root_key, Role::Bob, 23).unwrap();

        let fut_a = async {
            use futures::io::AsyncWriteExt;
            stream_a.write_all(b"the quick brown fox").await.unwrap();
            stream_a.flush().await.unwrap();
            stream_a
                .write_all(b" jumps over the lazy dog")
                .await
                .unwrap();
            stream_a.flush().await.unwrap();
            stream_a.close().await.unwrap();
        };
        let mut res = vec![];
        let fut_b = async {
            use futures::io::AsyncReadExt;
            stream_b.read_to_end(&mut res).await.unwrap();
        };
        block_on(async { join!(fut_a, fut_b) });
        assert_eq!(res, b"the quick brown fox jumps over the lazy dog");
    }
}
