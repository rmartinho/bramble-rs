//! Bramble transport  protocol, version 4
#![warn(missing_docs)]

mod clock;
mod connection;
mod crypto;
mod error;

pub use connection::Connection;
pub use error::{Error, Result};

const CURRENT_VERSION: u16 = 4;
