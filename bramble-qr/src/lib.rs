//! Bramble QR code protocol, version 4
#![warn(missing_docs)]

mod error;
mod protocol;
mod scan;

pub use error::{Error, Result};
pub use protocol::{perform_key_agreement, Commit};
pub use scan::{BluetoothDescriptor, Descriptor, LanDescriptor, Payload, UnknownDescriptor};

const CURRENT_VERSION: u8 = 4;
const BETA_VERSION: u8 = 89;
