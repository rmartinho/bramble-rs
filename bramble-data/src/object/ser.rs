//! BDF object serialization

use crate::{Error, Map, Object, Result};
use serde::{ser, Serialize};
use std::result;

/// Converts a value into a BDF [`Object`].
pub fn to_object<T>(value: T) -> Result<Object>
where
    T: Serialize,
{
    value.serialize(Serializer)
}

impl Serialize for Object {
    fn serialize<S>(&self, serializer: S) -> result::Result<S::Ok, S::Error>
    where
        S: ser::Serializer,
    {
        match self {
            Object::Null => serializer.serialize_unit(),
            Object::Boolean(value) => serializer.serialize_bool(*value),
            Object::Integer(value) => serializer.serialize_i64(*value),
            Object::Float(value) => serializer.serialize_f64(*value),
            Object::String(value) => serializer.serialize_str(value),
            Object::Raw(value) => serializer.serialize_bytes(value),
            Object::List(value) => value.serialize(serializer),
            Object::Map(value) => value.serialize(serializer),
        }
    }
}

/// Serializer that serializes into an [`Object`]
#[derive(Copy, Clone)]
pub struct Serializer;

impl ser::Serializer for Serializer {
    type Ok = Object;
    type Error = Error;

    type SerializeSeq = SerializeList;
    type SerializeTuple = SerializeList;
    type SerializeTupleStruct = SerializeList;
    type SerializeTupleVariant = SerializeListVariant;
    type SerializeMap = SerializeMap;
    type SerializeStruct = SerializeList;
    type SerializeStructVariant = SerializeListVariant;

    fn serialize_bool(self, value: bool) -> Result<Object> {
        Ok(Object::Boolean(value))
    }

    fn serialize_i8(self, value: i8) -> Result<Object> {
        self.serialize_i64(value as i64)
    }

    fn serialize_i16(self, value: i16) -> Result<Object> {
        self.serialize_i64(value as i64)
    }

    fn serialize_i32(self, value: i32) -> Result<Object> {
        self.serialize_i64(value as i64)
    }

    fn serialize_i64(self, value: i64) -> Result<Object> {
        Ok(Object::Integer(value))
    }

    fn serialize_u8(self, value: u8) -> Result<Object> {
        self.serialize_i64(value as i64)
    }

    fn serialize_u16(self, value: u16) -> Result<Object> {
        self.serialize_i64(value as i64)
    }

    fn serialize_u32(self, value: u32) -> Result<Object> {
        self.serialize_i64(value as i64)
    }

    fn serialize_u64(self, value: u64) -> Result<Object> {
        if value <= i64::MAX as u64 {
            self.serialize_i64(value as i64)
        } else {
            Err(Error::InvalidValue)
        }
    }

    fn serialize_f32(self, value: f32) -> Result<Object> {
        self.serialize_f64(value as f64)
    }

    fn serialize_f64(self, value: f64) -> Result<Object> {
        Ok(Object::Float(value))
    }

    fn serialize_char(self, value: char) -> Result<Object> {
        self.serialize_i64(value as i64)
    }

    fn serialize_str(self, value: &str) -> Result<Object> {
        Ok(Object::String(value.into()))
    }

    fn serialize_bytes(self, value: &[u8]) -> Result<Object> {
        Ok(Object::Raw(value.into()))
    }

    fn serialize_unit(self) -> Result<Object> {
        Ok(Object::Null)
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<Object> {
        self.serialize_unit()
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
    ) -> Result<Object> {
        self.serialize_u32(variant_index)
    }

    fn serialize_newtype_struct<T>(self, _name: &'static str, value: &T) -> Result<Object>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(self)
    }

    fn serialize_newtype_variant<T>(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        value: &T,
    ) -> Result<Object>
    where
        T: ?Sized + Serialize,
    {
        Ok(Object::List(vec![
            Serializer.serialize_u32(variant_index)?,
            value.serialize(self)?,
        ]))
    }

    fn serialize_none(self) -> Result<Object> {
        self.serialize_unit()
    }

    fn serialize_some<T>(self, value: &T) -> Result<Object>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(self)
    }

    fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq> {
        Ok(SerializeList {
            list: Vec::with_capacity(len.unwrap_or(0)),
        })
    }

    fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple> {
        self.serialize_seq(Some(len))
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleStruct> {
        self.serialize_tuple(len)
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        variant_index: u32,
        _variant: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleVariant> {
        use ser::SerializeTuple;
        let mut outer = self.serialize_tuple(2)?;
        outer.serialize_element(&variant_index)?;
        let inner = self.serialize_tuple(len)?;
        Ok(SerializeListVariant { outer, inner })
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        Ok(SerializeMap {
            map: Map::new(),
            next_key: None,
        })
    }

    fn serialize_struct(self, _name: &'static str, len: usize) -> Result<Self::SerializeStruct> {
        self.serialize_tuple(len)
    }

    fn serialize_struct_variant(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
        len: usize,
    ) -> Result<Self::SerializeStructVariant> {
        self.serialize_tuple_variant(name, variant_index, variant, len)
    }
}

pub struct SerializeList {
    list: Vec<Object>,
}

impl ser::SerializeSeq for SerializeList {
    type Ok = Object;
    type Error = Error;

    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.list.push(to_object(value)?);
        Ok(())
    }

    fn end(self) -> Result<Object> {
        Ok(Object::List(self.list))
    }
}

impl ser::SerializeTuple for SerializeList {
    type Ok = Object;
    type Error = Error;

    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        ser::SerializeSeq::serialize_element(self, value)
    }

    fn end(self) -> Result<Object> {
        ser::SerializeSeq::end(self)
    }
}

impl ser::SerializeTupleStruct for SerializeList {
    type Ok = Object;
    type Error = Error;

    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        ser::SerializeSeq::serialize_element(self, value)
    }

    fn end(self) -> Result<Object> {
        ser::SerializeSeq::end(self)
    }
}

impl ser::SerializeStruct for SerializeList {
    type Ok = Object;
    type Error = Error;

    fn serialize_field<T>(&mut self, _name: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        ser::SerializeSeq::serialize_element(self, value)
    }

    fn end(self) -> Result<Object> {
        ser::SerializeSeq::end(self)
    }
}

pub struct SerializeListVariant {
    inner: SerializeList,
    outer: SerializeList,
}

impl ser::SerializeTupleVariant for SerializeListVariant {
    type Ok = Object;
    type Error = Error;

    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        ser::SerializeTuple::serialize_element(&mut self.inner, value)
    }

    fn end(mut self) -> Result<Object> {
        let obj = ser::SerializeSeq::end(self.inner)?;
        ser::SerializeTuple::serialize_element(&mut self.outer, &obj)?;
        ser::SerializeSeq::end(self.outer)
    }
}

impl ser::SerializeStructVariant for SerializeListVariant {
    type Ok = Object;
    type Error = Error;

    fn serialize_field<T>(&mut self, _name: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        ser::SerializeTupleVariant::serialize_field(self, value)
    }

    fn end(self) -> Result<Object> {
        ser::SerializeTupleVariant::end(self)
    }
}

pub struct SerializeMap {
    map: Map,
    next_key: Option<String>,
}

impl ser::SerializeMap for SerializeMap {
    type Ok = Object;
    type Error = Error;

    fn serialize_key<T>(&mut self, key: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.next_key = Some(key.serialize(KeySerializer)?);
        Ok(())
    }

    fn serialize_value<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        // panic: this is a bug in the program, not an expected failure
        let key = self
            .next_key
            .take()
            .expect("serialize_value called before serialize_key");
        self.map.insert(key, to_object(value)?);
        Ok(())
    }

    fn end(self) -> Result<Object> {
        Ok(Object::Map(self.map))
    }
}

struct KeySerializer;

impl ser::Serializer for KeySerializer {
    type Ok = String;
    type Error = Error;

    type SerializeSeq = ser::Impossible<String, Error>;
    type SerializeTuple = ser::Impossible<String, Error>;
    type SerializeTupleStruct = ser::Impossible<String, Error>;
    type SerializeTupleVariant = ser::Impossible<String, Error>;
    type SerializeMap = ser::Impossible<String, Error>;
    type SerializeStruct = ser::Impossible<String, Error>;
    type SerializeStructVariant = ser::Impossible<String, Error>;

    fn serialize_bool(self, _value: bool) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_i8(self, _value: i8) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_i16(self, _value: i16) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_i32(self, _value: i32) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_i64(self, _value: i64) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_u8(self, _value: u8) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_u16(self, _value: u16) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_u32(self, _value: u32) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_u64(self, _value: u64) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_f32(self, _value: f32) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_f64(self, _value: f64) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_char(self, _value: char) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_str(self, value: &str) -> Result<String> {
        Ok(value.into())
    }

    fn serialize_bytes(self, _value: &[u8]) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_unit(self) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
    ) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_newtype_struct<T>(self, _name: &'static str, _value: &T) -> Result<String>
    where
        T: ?Sized + Serialize,
    {
        Err(Error::WrongType)
    }

    fn serialize_newtype_variant<T>(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _value: &T,
    ) -> Result<String>
    where
        T: ?Sized + Serialize,
    {
        Err(Error::WrongType)
    }

    fn serialize_none(self) -> Result<String> {
        Err(Error::WrongType)
    }

    fn serialize_some<T>(self, _value: &T) -> Result<String>
    where
        T: ?Sized + Serialize,
    {
        Err(Error::WrongType)
    }

    fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
        Err(Error::WrongType)
    }

    fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
        Err(Error::WrongType)
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleStruct> {
        Err(Error::WrongType)
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleVariant> {
        Err(Error::WrongType)
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        Err(Error::WrongType)
    }

    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        Err(Error::WrongType)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStructVariant> {
        Err(Error::WrongType)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use serde::Serialize;
    use std::collections::HashMap;

    #[test]
    fn to_object_maps() {
        let mut map = HashMap::new();
        map.insert("foo", 123u32);
        map.insert("bar", 456u32);
        let obj = to_object(&map).unwrap();

        let mut expected = Map::new();
        expected.insert("foo".to_string(), Object::Integer(123));
        expected.insert("bar".to_string(), Object::Integer(456));
        assert_eq!(Object::Map(expected), obj);
    }

    #[test]
    fn to_object_structs() {
        #[derive(Serialize)]
        struct Test {
            x: bool,
            y: u32,
            z: Vec<String>,
        }

        let s = Test {
            x: true,
            y: 17,
            z: vec!["foo".into(), "bar".into()],
        };
        let obj = to_object(&s).unwrap();
        assert_eq!(
            Object::List(vec![
                Object::Boolean(true),
                Object::Integer(17),
                Object::List(vec![
                    Object::String("foo".into()),
                    Object::String("bar".into())
                ])
            ]),
            obj
        );
    }

    #[test]
    fn to_object_enums() {
        #[derive(Serialize)]
        enum Test {
            UnitVariant,
            NewTypeVariant(u32),
            TupleVariant(bool, u32),
            StructVariant { x: bool, y: u32 },
        }

        let e = Test::UnitVariant;
        let obj = to_object(&e).unwrap();
        assert_eq!(Object::Integer(0), obj);

        let e = Test::NewTypeVariant(17);
        let obj = to_object(&e).unwrap();
        assert_eq!(
            Object::List(vec![Object::Integer(1), Object::Integer(17)]),
            obj
        );

        let e = Test::TupleVariant(true, 17);
        let obj = to_object(&e).unwrap();
        assert_eq!(
            Object::List(vec![
                Object::Integer(2),
                Object::List(vec![Object::Boolean(true), Object::Integer(17)])
            ]),
            obj
        );

        let e = Test::StructVariant { x: true, y: 17 };
        let obj = to_object(&e).unwrap();
        assert_eq!(
            Object::List(vec![
                Object::Integer(3),
                Object::List(vec![Object::Boolean(true), Object::Integer(17)])
            ]),
            obj
        );
    }

    #[test]
    fn to_object_options() {
        let o: Option<u32> = None;
        let obj = to_object(&o).unwrap();
        assert_eq!(Object::Null, obj);

        let o = Some(17);
        let obj = to_object(&o).unwrap();
        assert_eq!(Object::Integer(17), obj);
    }
}
