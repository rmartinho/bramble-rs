//! Binary data format, version 1
#![warn(missing_docs)]

mod constants;
mod de;
mod error;
mod object;
mod ser;

pub mod custom;

pub use de::{from_reader, from_slice, Deserializer, StreamDeserializer};
pub use error::{Error, Result};
pub use object::{from_object, to_object, Object, Serializer as ObjectSerializer};
pub use ser::{to_vec, to_writer, Serializer};

/// A BDF map type.
pub type Map = std::collections::BTreeMap<String, Object>;
