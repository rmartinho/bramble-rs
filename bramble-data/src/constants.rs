//! BDF constants

pub const TYPE_MASK: u8 = 0xF0;
pub const TYPE_NULL: u8 = 0x00;
pub const TYPE_BOOLEAN: u8 = 0x10;
pub const TYPE_INTEGER: u8 = 0x20;
pub const TYPE_FLOAT: u8 = 0x30;
pub const TYPE_STRING: u8 = 0x40;
pub const TYPE_RAW: u8 = 0x50;
pub const TYPE_LIST: u8 = 0x60;
pub const TYPE_DICTIONARY: u8 = 0x70;
pub const TYPE_END: u8 = 0x80;
