//! Bramble handshake protocol, version 0
#![warn(missing_docs)]

mod error;
mod protocol;

pub use error::{Error, Result};
pub use protocol::perform_handshake;

const CURRENT_VERSION: u8 = 0;
const BETA_VERSION: u8 = 89;
