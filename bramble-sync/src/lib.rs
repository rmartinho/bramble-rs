//! Bramble synchronization protocol, version 0
#![warn(missing_docs)]

mod error;

pub use error::{Error, Result};

#[allow(dead_code)]
const CURRENT_VERSION: u8 = 0;
#[allow(dead_code)]
const BETA_VERSION: u8 = 89;
